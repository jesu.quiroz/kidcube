from django.contrib import admin
from apps.usuario.models import Usuario, Preguntas, Respuestas, Usuario_Pregunta, Comentario, Usuario_Nivel, Niveles
# Register your models here.

admin.site.register(Usuario)
admin.site.register(Preguntas)
admin.site.register(Respuestas)
admin.site.register(Usuario_Pregunta)
admin.site.register(Comentario)
admin.site.register(Usuario_Nivel)
admin.site.register(Niveles)
