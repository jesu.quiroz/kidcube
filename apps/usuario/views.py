from django.shortcuts import render, redirect
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import authenticate
from django.contrib.auth import login as do_login
from django.contrib.auth import logout as do_logout
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import models
from apps.usuario.models import Usuario
from django.contrib.auth.models import User
# Create your views here.

def home(request):
    return render(request,'usuario/index.html')

def registro(request):
    if request.POST:
        
        nombre = request.POST.get('nombre',False)
        apellido = request.POST.get('apellido',False)
        f_nacimiento = request.POST.get('f_nacimiento',False)
        correo= request.POST.get('correo',False)
        clave = request.POST.get('clave',False)
        experiencia = 0

        v = Usuario( nombre=nombre, apellido=apellido, f_nacimiento=f_nacimiento, correo=correo, clave=clave, experiencia=experiencia)
        u = User.objects.create_user(
        username=correo,password=clave
        )
        v.save()
        u.save()
        return render(request, "usuario/index.html")

    
    return render(request, "usuario/registro.html")

def logout(request):
    # Finalizamos la sesión
    do_logout(request)
    # Redireccionamos a la portada
    return redirect('/')
    
def login(request):
    # Creamos el formulario de autenticación vacío
    form = AuthenticationForm()
    if request.method == "POST":
        # Añadimos los datos recibidos al formulario
        form = AuthenticationForm(data=request.POST)
        # Si el formulario es válido...
        if form.is_valid():
            # Recuperamos las credenciales validadas
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            # Verificamos las credenciales del usuario
            user = authenticate(username=username, password=password)

            # Si existe un usuario con ese nombre y contraseña
            if user is not None:
                # Hacemos el login manualmente
                do_login(request, user)
                # Y le redireccionamos a la portada
                return redirect('/main.html')

    # Si llegamos al final renderizamos el formulario
    return render(request, "usuario/login.html", {'form': form})
    

def menu(request):
    return render(request,'usuario/menu.html')

def main(request):
    return render(request,'usuario/main.html')
