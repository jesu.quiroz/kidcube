from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.urls import path
from apps.usuario.views import home,registro,login,menu,main
from django.contrib.auth.views import logout_then_login


urlpatterns = [
    path('', home,name="home"),
    path('index.html/', login_required(home),name="home"), #Url (si esta vacio es el home), la vista
    path('registro.html/',registro,name="registro"),
    path('accounts/login/',login,name='login'),
    path('menu.html/',menu,name='menu'),
    path('main.html/',main,name='main'),
    #path('login.html/', login),
]