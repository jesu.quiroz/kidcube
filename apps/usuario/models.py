from django.db import models

# Create your models here.

class Usuario(models.Model):
    id_usuario=models.CharField(max_length=10,primary_key=True)
    nombre=models.CharField('nombre',max_length=50)
    apellido=models.CharField('apellido',max_length=50)
    f_nacimiento=models.DateField('f_nacimiento',auto_now_add=True)
    correo=models.CharField('correo',max_length=60)
    clave=models.CharField('clave',max_length=10)
    experiencia=models.IntegerField('experiencia')
 
    class Meta:
        verbose_name = 'Usuario'
        verbose_name_plural = 'Usuarios'

    def __str__(self):
        return self.correo


class Preguntas(models.Model):
    id_pregunta=models.CharField(max_length=10,primary_key=True)
    pregunta=models.CharField(max_length=100)
    tiempo=models.IntegerField()
    puntaje=models.IntegerField()

class Respuestas(models.Model):
    id_respuesta=models.CharField(max_length=10,primary_key=True)
    respuesta=models.CharField(max_length=100)
    id_pregunta=models.ForeignKey(Preguntas,blank=True,null=True,on_delete=models.CASCADE)
    correcta=models.BooleanField()

class Usuario_Pregunta(models.Model):
    id_usuario_pregunta=models.CharField(max_length=10,primary_key=True)
    id_usuario=models.ForeignKey(Usuario,blank=True,null=True,on_delete=models.CASCADE)
    id_pregunta=models.ForeignKey(Preguntas,blank=True,null=True,on_delete=models.CASCADE)
    fecha=models.DateField(auto_now_add=True)
    valor_ptos_obtenidos=models.IntegerField()

class Comentario(models.Model):
    id_comentario=models.CharField(max_length=10,primary_key=True)
    id_usuario=models.ForeignKey(Usuario,blank=True,null=True,on_delete=models.CASCADE)
    fecha_coment=models.DateTimeField(auto_now_add=True)
    comentario=models.CharField(max_length=200)

class Usuario_Nivel(models.Model):
    id_nivel=models.CharField(max_length=8,primary_key=True)
    id_usuario=models.ForeignKey(Usuario,blank=True,null=True,on_delete=models.CASCADE)
    ptje_obtenido=models.IntegerField()


class Niveles(models.Model):
    id_nivel=models.CharField(max_length=8,primary_key=True)
    nombre=models.CharField(max_length=10)
    exp_min=models.IntegerField()
    exp_max=models.IntegerField()