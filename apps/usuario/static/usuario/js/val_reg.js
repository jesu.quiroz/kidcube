$(document).ready(function(){
   
    validation = new Date()
    validation.setFullYear(validation.getFullYear() -6)
    format= `${validation.getFullYear()}-${(validation.getMonth()+1)}-${validation.getDate()}`
    console.log(format)

    $("#formulario").validate({

       
        errorClass:"is-invalid",
        rules:{
            nombre:{
                required:true
            },
            ape:{
                required:true
            },
            correo:{
                required:true,
                email:true
            },
            contrasena:{
                required: true
                
            },
            fnacimiento:{
                required:true,
                max:format
                }
        },
        messages:{
            nombre:{
                required:"Debe ingresar su nombre"
            },
            nombre:{
                required:"Debe ingresar su apellido"
            },
            correo:{
                required:"Ingrese un correo",
                email:"Ingrese un correo valido por favor"
            },
            contrasena:{
                required:"Ingrese su clave"
            },
            fnacimiento:{
                required:"Debes ingresar tu fecha de nacimiento",
                max:"Debes ser mayor 6 años para registrarte"
            }
        }
    })
})

$('#text_3').keyup(function () {
 
    
 
    
 
});

$("#formulario").submit(function(){
    if($("#formulario").valid()){
        return true
    }else{
        Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Algo a salido mal!',
            //footer: '<a href>Why do I have this issue?</a>'
          })
    }
    return false
})