$(document).ready(function(){

    $("#formulario").validate({
        errorClass:"is-invalid",
        rules:{
            correo:{
                required:true,
                email: true
            },
            contrasena:{
                required: true,
            }
        },
        messages:{
            correo:{
                required:"Debe ingresar su Correo"
            },
            contrasena:{
                required:"Ingrese su Contraseña"
            }
        }
    })
})
 
$("#formulario").submit(function(){
    if($("#formulario").valid()){
        return true
    }else{
        Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Algo a salido mal!',
            //footer: '<a href>Why do I have this issue?</a>'
          })
    }
    return false
})